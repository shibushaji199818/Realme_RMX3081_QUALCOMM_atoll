{
    "Image_Build_IDs": {
        "adsp": "ADSP.VT.5.3-00308-SM6150-3", 
        "aop": "AOP.HO.2.0.3-00051-RENNELLAAAAANAZO-3", 
        "boot": "BOOT.XF.3.1-00537-SM6150LZB-2", 
        "btfm_CHK": "BTFM.CHE.2.1.5-00254-QCACHROMZ-1", 
        "cdsp": "CDSP.VT.2.3-00189-SM6150-1", 
        "common": "Rennell.LA.2.0-00021-STD.PROD-1", 
        "glue": "GLUE.RENNELL_LA.2.1-00023-NOOP_TEST-1", 
        "modem": "MPSS.AT.4.4-00286-RENNELL_GEN_PACK-1", 
        "npu": "NPU.FW.2.2-00061-RENNELL_NPU_PACK-1", 
        "tz": "TZ.XF.5.0.3-00259-S6150AAAAANAZT-1", 
        "tz_apps": "TZ.APPS.2.0-00086-S6150AAAAANAZT-1.456542.3.471767.1", 
        "video": "VIDEO.VE.5.4-00026-PROD-1", 
        "wlan": "WLAN.HL.3.2.2-00566-QCAHLSWMTPLZ-1"
    }, 
    "Metabuild_Info": {
        "Meta_Build_ID": "Rennell.LA.2.0-00021-STD.PROD-1", 
        "Product_Flavor": "asic", 
        "Time_Stamp": "2022-09-19 20:37:04"
    }, 
    "Version": "1.0"
}